CREATE DATABASE empresa;
	
CREATE TABLE ct_departamento
(
	cdid integer NOT NULL AUTO_INCREMENT,
	dename varchar (15) NOT NULL,

	CONSTRAINT pk_departamento PRIMARY KEY (cdid)
);

CREATE TABLE ct_funcionario
(
	cdid integer NOT NULL AUTO_INCREMENT,
	nmnome varchar(30) NOT NULL, 
	decargo varchar(30),
	nuidade integer, 
	qtsalario real,
	cdidDepartamento integer NOT NULL,
	
	CONSTRAINT pk_funcionario PRIMARY KEY (cdid),
	CONSTRAINT fk_funcionario_departamento FOREIGN KEY (cdidDepartamento) REFERENCES ct_departamento (cdid)
);