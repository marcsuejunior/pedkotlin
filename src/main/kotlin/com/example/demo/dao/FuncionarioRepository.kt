package com.example.demo.dao

import javax.transaction.Transactional
import org.springframework.stereotype.Repository
import org.springframework.data.repository.CrudRepository
import com.example.demo.model.Funcionario
import org.springframework.data.jpa.repository.Query

@Transactional
@Repository
interface FuncionarioRepository : CrudRepository <Funcionario, Long> 
{
	@Query(value="SELECT empresa.ct_funcionario.cdid, empresa.ct_funcionario.nmnome, empresa.ct_funcionario.decargo, empresa.ct_funcionario.nuidade, empresa.ct_funcionario.qtsalario, empresa.ct_funcionario.cdidDepartamento \n"+
			"		FROM empresa.ct_funcionario INNER JOIN empresa.ct_departamento ON (empresa.ct_funcionario.cdidDepartamento = empresa.ct_departamento.cdid)WHERE empresa.ct_departamento.cdid = ?1 ;",
			nativeQuery=true)
	fun findFuncionariosOndDpto (dpto: Int) : List<Funcionario>;
}