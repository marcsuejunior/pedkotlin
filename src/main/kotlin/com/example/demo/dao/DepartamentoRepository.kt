package com.example.demo.dao

import org.springframework.data.repository.CrudRepository
import  com.example.demo.model.Departamento
import org.springframework.stereotype.Repository
import javax.transaction.Transactional
import org.springframework.data.jpa.repository.Query

@Transactional
@Repository
interface DepartamentoRepository : CrudRepository <Departamento,Long> 
{
}