package com.example.demo

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.boot.SpringApplication

@SpringBootApplication
open class KotlinApplication

fun main(args: Array<String>)
{
    SpringApplication.run(KotlinApplication::class.java, *args)
}
