package com.example.demo.model


import javax.persistence.Table
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Column
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType

@Entity
@Table(name="ct_departamento")
class Departamento
{
	@Id
	@Column(name="cdid")
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	var id : Long = 0
	
	@Column(name="dename")
	
	var name : String = ""  
	
	override fun toString () : String
	{
		return "id: "+id +" name: "+name
	}  
}