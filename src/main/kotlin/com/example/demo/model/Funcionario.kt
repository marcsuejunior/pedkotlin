package com.example.demo.model


import javax.persistence.Table
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Column
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType


@Entity
@Table (name="ct_funcionario", schema="empresa")
class Funcionario
{
	@Id
	@Column(name="cdid")
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	var id : Long = 0
	
	@Column(name="nmnome")
	var name : String = ""

	@Column(name="decargo")
	var cargo : String = ""
	
	@Column(name="nuidade")
	var idade : Int = 0
	
	@Column(name="qtsalario")
	var salario : Double = 0.0
	
	@Column(name="cdiddepartamento")
	var idDepartamento : Long = 0
	
	override fun toString () : String
	{
		return "id: "+id +" name: "+name +" dpto: "+idDepartamento
	} 
}


