package com.example.demo.controller

import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.beans.factory.annotation.Autowired
import com.example.demo.dao.FuncionarioRepository
import com.example.demo.model.Funcionario
import org.springframework.web.bind.annotation.PathVariable
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.RequestBody

@RestController
@RequestMapping ("/funcionario")
class FuncionarioController
{
	val log = LoggerFactory.getLogger(FuncionarioController::class.java)
	
	@Autowired
	lateinit var funcionarioRepository : FuncionarioRepository
	
	@RequestMapping ("/findById/{id}")
	fun findById(@PathVariable id : Long) : Funcionario = funcionarioRepository.findById(id).orElse(Funcionario())
	
	@RequestMapping(value="/findFuncionariosOnDpto/{idDpto}")
	fun findFuncionariosOnDepartamento (@PathVariable idDpto : Int) : List<Funcionario>
	{
		return funcionarioRepository.findFuncionariosOndDpto(idDpto);
	}
	
	@RequestMapping("/save", method=[RequestMethod.POST] )
	fun save(@RequestBody funcionario : Funcionario) : Funcionario
	{
		try
		{
			if (funcionario.name.equals(""))
				throw IllegalArgumentException("Campo Nome e obrigatorio")
			else if (funcionario.idDepartamento==0L)
				throw IllegalArgumentException("Campo Departamento e obrigatorio")
			else
				return funcionarioRepository.save(funcionario)
				
		}
		catch (e : Exception)
		{
			log.error(e.toString())
			return Funcionario()
		}
	}
	
	@RequestMapping("/update", method=[RequestMethod.POST] )
	fun update(@RequestBody funcionario : Funcionario) : Funcionario
	{
		try
		{
			var funcionarioTeste = Funcionario()
			
			if (funcionario.id == 0L)
				throw IllegalArgumentException("Campo ID obrigatorio")
			else if (funcionario.name.equals(""))
				throw IllegalArgumentException("Campo Nome obrigatorio")
			else if (funcionario.idDepartamento==0L)
				throw IllegalArgumentException("Campo Departamento obrigatorio")
			else
				return funcionarioRepository.save(funcionario)
				
		}
		catch (e : Exception)
		{
			log.error(e.toString())
			return Funcionario()
		}
	}
	
	@RequestMapping("/delete/{id}", method=[RequestMethod.DELETE])
	fun delete (@PathVariable id : Long) : Boolean
	{
		var func : Funcionario = findById(id)
		
		if (func.id!=0L)
		{
			funcionarioRepository.delete(func)
			var isDeleted : Funcionario = findById(id)
		
			if (isDeleted.id==0L)
				return true
			else
				return false
		}
		return false
	}
	
}