package com.example.demo.controller

import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import com.example.demo.model.Departamento
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.beans.factory.annotation.Autowired
import com.example.demo.dao.DepartamentoRepository
import org.springframework.stereotype.Controller
import org.slf4j.LoggerFactory
import java.util.logging.Logger
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.RequestBody

@RestController
@RequestMapping("/departamento")
class DepartamentoController ()
{
	val log = LoggerFactory.getLogger(DepartamentoController::class.java)
	
	@Autowired
	lateinit var departamentoRepository : DepartamentoRepository
	
	@RequestMapping("/findById/{id}")
	fun buscaDepartamento (@PathVariable id : Long) : Departamento
	{
		return departamentoRepository.findById(id).orElse(Departamento())
	}
	
	@RequestMapping("/save", method=[RequestMethod.POST] )
	fun save(@RequestBody deparatmento : Departamento) : Departamento
	{
		try
		{
			if (deparatmento.name.equals(""))
				throw IllegalArgumentException("Campo nome obrigatorio")
			else
				return departamentoRepository.save(deparatmento)

		}
		catch (e : Exception)
		{
			log.error(e.toString())
			return Departamento()
		}
	}
	
	@RequestMapping("/update", method=[RequestMethod.POST])
	fun update(@RequestBody deparatmento : Departamento) : Departamento
	{
		try
		{
			if (deparatmento.id == 0L)
				throw IllegalArgumentException("Campo ID obrigatorio")
			else if (deparatmento.name.equals(""))
				throw IllegalArgumentException("Campo Nome obrigatorio")
			else
				return departamentoRepository.save(deparatmento)
		}
		catch (e : Exception)
		{
			log.error(e.toString())
			return Departamento()
		}
	}	
}

